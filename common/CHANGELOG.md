# Revision history for ghc-debug-common

## 0.2.1.0 -- 2022-05-06

* Internal refactoring to give better error message on decoding failure

## 0.2.0.0 -- 2021-12-06

* Second version

## 0.1.0.0 -- 2021-06-14

* First version.
